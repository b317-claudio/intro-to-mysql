SELECT customerName FROM customers WHERE country = "Philippines";

SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName FROM customers WHERE state IS NULL;

SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

SELECT * FROM productlines WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT country FROM customers;

SELECT DISTINCT status FROM orders;

SELECT customerName, country FROM customers where country = "USA" OR country = "France" OR country = "Canada";

SELECT firstName, lastName, city FROM offices JOIN employees ON offices.officeCode = employees.officeCode WHERE city = "Tokyo";

SELECT customerName FROM customers JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber WHERE firstName = "Leslie" AND lastName = "Thompson";

SELECT products.productName, customers.customerName FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
JOIN orders ON orders.orderNumber = orderdetails.orderNumber
JOIN customers ON customers.customerNumber = orders.customerNumber
WHERE customerName = "Baane Mini Imports";

SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM employees
JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
JOIN offices ON employees.officeCode = offices.officeCode
WHERE customers.country = offices.country;

SELECT productName, quantityInStock FROM products WHERE productLine = "planes" AND quantityInStock < 1000;

SELECT customerName FROM customers WHERE phone LIKE "+81%";